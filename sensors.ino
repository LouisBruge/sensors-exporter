#include <dht_nonblocking.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <SPI.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

#define DHT_SENSOR_TYPE DHT_TYPE_11
#define SEA_LEVEL_PRESURE_HPA (1016.9)

const int SENSOR_18B20_PIN = 4;

// data pin for dht sensor
const int DHT_SENSOR_PIN = 2;

// data pin for thermistor sensor
const int THERMISTOR_SENSOR_PIN = A0;

// Resistor for thermistor sensor
const int THERMISTOR_RESISTOR = 10000;

// Constant for thermistor mapping
const float c1 = 0.001129148, c2 = 0.000234125, c3 = 0.0000000876741;

// data pin for photocell sensor
const int PHOTOCELL_SENSOR_PIN = A3;

OneWire oneWire(SENSOR_18B20_PIN);
DallasTemperature sensor18b20(&oneWire);

DHT_nonblocking dht_sensor(DHT_SENSOR_PIN, DHT_SENSOR_TYPE);

Adafruit_BME280 bme280;

// declaration of functions definitions
static bool measure_env(float *temperature, float *humidity);
float thermistor_measure_celsius();
float brightness_measure();

String sensor_metric_to_json(String sensor_name, String kind, float metric);

void setup ()
{
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.begin(9600);

  while(!bme280.begin(0x76)) {
    Serial.println("Could find a valid BME280 sensors at 0x76 addr, check wiring!");
    delay(1000);
  }

  digitalWrite(LED_BUILTIN, HIGH);
  delay(3000); // wait for console opening
  Serial.println("Ping");
  digitalWrite(LED_BUILTIN, LOW);
}

/**
   loop execute the main process

   @return {void}
*/
void loop ()
{
  float temperature;
  float humidity;

  if (measure_env(&temperature, &humidity) == true)
  {

    digitalWrite(LED_BUILTIN, HIGH);

    // DHT 11 metrics
    Serial.println(sensor_metric_to_json("DHT_11", "humidity", humidity));
    Serial.println(sensor_metric_to_json("DHT_11", "temperature", temperature));

    // PHOTOCELL sensor metrics
    Serial.println(sensor_metric_to_json("PHOTOCELL", "brightness", brightness_measure()));

    // THERMISTOR sensor metrics
    Serial.println(sensor_metric_to_json("THERMISTOR", "temperature", thermistor_measure_celsius()));

    // 18B20 sensor metrics
    sensor18b20.requestTemperatures();
    Serial.println(sensor_metric_to_json("18B20", "temperature", sensor18b20.getTempCByIndex(0)));

    // BME280 sensor metrics
    Serial.println(sensor_metric_to_json("BME280", "temperature", bme280.readTemperature()));
    Serial.println(sensor_metric_to_json("BME280", "humidity", bme280.readHumidity()));
    Serial.println(sensor_metric_to_json("BME280", "pressure", bme280.readPressure() / 100.0F));

    digitalWrite(LED_BUILTIN, LOW);
  }
}

/**
   measure_env set temperature and humidity metrics measured from the DHT 11 sensor
   if return true if mesure had been read, false if not

   @param {float*} temperature - temperature
   @param {float*} humidity - humidity
   @returns {bool}
*/
static bool measure_env(float *temperature, float *humidity)
{
  static unsigned long measurement_timestamp = millis();
  if ( millis() - measurement_timestamp > 3000ul)
  {
    if (dht_sensor.measure(temperature, humidity) == true)
    {
      measurement_timestamp = millis();
      return true;
    }
  }
  return false;
}

/**
   brightness_measure measure the brightness of the piece
   in return value on percentage

   @return {float}
*/
float brightness_measure() {
  int rawValue = analogRead(PHOTOCELL_SENSOR_PIN);
  int brightness = map(1023 - rawValue, 1023, 0, 0, 100);
  return (float)brightness;
}

/**
   thermistor_measure_celsius returns celsius temperature measure by the thermistor sensor

   @returns {float}
*/
float thermistor_measure_celsius()
{
  int Vo = analogRead(THERMISTOR_SENSOR_PIN); // volt on output of the sensor
  float R2 = log(THERMISTOR_RESISTOR * ((1024.0 / (float)Vo - 1.0))); // resistance on the ouput
  float T = (1.0 / (c1 + (c2 + (c3 * R2 * R2)) * R2));
  return T - 273.15;
}

/**
   sensor_metric_to_json format ouput for sensor metrics

   @param {String} sensor_name - name of the sensor
   @param {String} kind - kind of sensor (measure)
   @param {float} metric - metric value
   @return {String}
*/
String sensor_metric_to_json(String sensor_name, String kind, float metric)
{
  return String(
           "{\"timestamp\":" + String(millis(), DEC)
           + ", \"metrics\":["
           + "{\"kind\": \"" + kind + "\", \"sensor\": \"" + sensor_name + "\", \"value\": " + String(metric, 1) + "}"
           + "]}"
         );
}
