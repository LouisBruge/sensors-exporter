module gitlab.com/LouisBruge/sensors-exporter

go 1.14

require (
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/go-logr/logr v0.2.0
	github.com/go-logr/stdr v0.2.0
	github.com/influxdata/influxdb1-client v0.0.0-20191209144304-8bf82d3c094d
	github.com/jinzhu/configor v1.1.1
	github.com/kr/pretty v0.2.0 // indirect
	github.com/prometheus/client_golang v1.7.1
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/net v0.0.0-20200822124328-c89045814202 // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
