package models

// Config defines main config
type Config struct {
	Collector CollectorConfig `json:"influx_db" yaml:"influx_db" required:"true"`
	MQTT      MQTTConfig      `json:"mqtt" yaml:"mqtt" required:"true"`
	Arduino   ArduinoConfig   `json:"arduino" yaml:"arduino"`
	Location  string          `json:"location" yaml:"location" env:"LOCATION" default:"undefined"`
	LogLevel  int             `json:"log_level" yaml:"log_level" env:"LOG_LEVEL" default:"1"`
	HTTP      HTTPConfig      `json:"http" yaml:"http"`
}

// CollectorConfig defines config needed to create a new collector
type CollectorConfig struct {
	Addr      string `json:"addr" yaml:"addr" env:"INFLUXDB_ADDR" default:"http://localhost:8086"`
	Precision string `json:"precision" yaml:"precision" env:"INFLUXDB_PRECISION" default:"m"`
	Database  string `json:"database" yaml:"database" env:"INFLUXDB_DATABASE" required:"true"`
}

// ArduinoConfig defines configuration options to communicate with the arduino board through serial port serie. Include
// also metadata information about the arduino board
type ArduinoConfig struct {
	Version    string `json:"version" yaml:"version" env:"ARDUINO_VERSION" default:"undefined"`
	Maker      string `json:"maker" yaml:"maker" env:"ARDUINO_MAkER" default:"undefined"`
	SerialPort string `json:"serial_port" yaml:"serial_port" env:"ARDUINO_SERIAL_PORT"`
	Baud       int    `json:"baud" yaml:"baud" env:"ARDUINO_BAUD" default:"9600"`
}

// MQTTConfig defines config needed for MQTT server
type MQTTConfig struct {
	Addr  string `json:"addr" yaml:"addr" env:"MQTT_ADDR" default:"tcp://localhost:1883"`
	Topic string `json:"topic" yaml:"topic" env:"MQTT_TOPIC" required:"true"`
}

// HTTPConfig defines config need for the HTTP Server
type HTTPConfig struct {
	Host string `json:"host" yaml:"host" env:"HTTP_HOST" default:"127.0.0.1"`
	Port int    `json:"port" yaml:"port" env:"HTTP_PORT" default:"8080"`
}
