package models

// Measure defines a measure object payload received for serial port
type Measure struct {
	Timestamp int64    `json:"timestamp"`
	Metrics   []Metric `json:"metrics"`
}

// Metric defines sensor's metric payload
type Metric struct {
	Location string  `json:"location"`
	Kind     string  `json:"kind"`
	Sensor   string  `json:"sensor"`
	Value    float32 `json:"value"`
}
