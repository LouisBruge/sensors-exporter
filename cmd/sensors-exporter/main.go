package main

import (
	"flag"
	"log"
	"os"
	"time"

	_ "net/http/pprof"

	"github.com/go-logr/stdr"
	"github.com/jinzhu/configor"
	"gitlab.com/LouisBruge/sensors-exporter/models"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector/influx"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/exporter"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/server"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/subscriber/mqtt"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/subscriber/serial"
)

func main() {
	configPath := flag.String("config", "config.yaml", "config file path")

	flag.Parse()

	logger := stdr.New(log.New(os.Stdout, "", log.LstdFlags|log.Lshortfile))

	var config models.Config
	logger.Info("try to load config", "path", configPath)
	if e := configor.Load(&config, *configPath); e != nil {
		logger.Error(e, "failed to load config")
		os.Exit(1)
	}

	stdr.SetVerbosity(config.LogLevel)

	ex, err := exporter.NewExporter(
		config,
		exporter.OptionWithLogger(logger),
		exporter.OptionWithSubscribers(
			serial.NewSerial(
				serial.OptionWithBaud(config.Arduino.Baud),
				serial.OptionWithPort(config.Arduino.SerialPort),
				serial.OptionWithLogger(logger),
			),
			mqtt.NewMQTT(
				mqtt.OptionWithAddress(config.MQTT.Addr),
				mqtt.OptionWithTopics(config.MQTT.Topic),
				mqtt.OptionWithLogger(logger),
			),
		),
		exporter.OptionWithCollector(
			influx.NewInfluxCollector(
				influx.OptionWithAddress(config.Collector.Addr),
				influx.OptionWithDatabase(config.Collector.Database),
				influx.OptionWithPrecision(config.Collector.Precision),
				influx.OptionWithTimeout(time.Second*30),
				influx.OptionWithLogger(logger),
			),
		),
	)
	if err != nil {
		logger.Error(err, "failed to load config")
		os.Exit(1)
	}

	s := server.NewHTTPServer(
		server.OptionWithHost(config.HTTP.Host),
		server.OptionWithPort(config.HTTP.Port),
	)

	go s.Listen()

	ex.Listen()
}
