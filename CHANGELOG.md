# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- flag option for the configuration file path
- systemd unit file
- installation script

### Changed
- rename default binary name

## [v0.0.1] - 2020-09-12
### Added
- Influx collector
- Arduino exporter through serial usb port
- exporter through MQTT server

[Unreleased]: https://gitlab.com/LouisBruge/sensors-exporter/-/compare/master...v0.0.1
[v0.0.1]: https://gitlab.com/LouisBruge/sensors-exporter/-/tags/v0.0.1
