# Sensors Export
[![Go Report
Card](https://goreportcard.com/badge/gitlab.com/LouisBruge/sensors-exporter)](https://goreportcard.com/report/gitlab.com/LouisBruge/sensors-exporter)
[![pipeline status](https://gitlab.com/LouisBruge/sensors-exporter/badges/master/pipeline.svg)](https://gitlab.com/LouisBruge/sensors-exporter/-/commits/master)
[![coverage report](https://gitlab.com/LouisBruge/sensors-exporter/badges/master/coverage.svg)](https://gitlab.com/LouisBruge/sensors-exporter/-/commits/master)    

Golang application that exports sensors metrics send by arduino over serial console to influxDB database

## Table of Content
1. [Technologies](#technologies)
2. [Requirement](#requirement)
3. [Installation](#installation)
4. [Usage](#usage)
5. [Configuration](#configuration)

## Technologies
  - Arduino
  - Golang
  - InfluxDB
  - Docker

## Requirement
### Software
| Stack | Version |
| ---: | :------ |
| Golang | go1.14 |
| Docker | 19.03.12-ce |
| docker-compose | 1.26.2 |
| InfluxDB | 1.8.1 |

### Hardware
#### Arduino board
Arduino board like tested:
- ELEGOO MEGA2650 R3
- ELEGOO UNO R3
- ARDUINO UNO REV 3

#### Sensors
- DHT11 temperature sensors
- Photocell sensor with 1K Ohm resitor
- Thermistor sensor with 1k Ohm resitor
- BME/BMP280 sensor

## Installation
### Local installation
1. Clone the project
  ```bash
  git clone https://gitlab.com/LouisBruge/sensors-exporter.git
  ```

2. Build the binary
  ```bash
  make build
  ```

3. Create the file config (see [configuration section](#configuration))
  ```bash
  cp config/config.example.yaml config.yaml
  nano config.yaml
  ```

4. Run the binary
  ```bash
  ./sensors-exporter
  ```

### System installation (systemd)

1. Clone the project
  ```bash
  git clone https://gitlab.com/LouisBruge/sensors-exporter.git
  ```

2. Build the binary
  ```bash
  make build
  ```

3. Run the installation script as root
   ```bash
   sudo make install
   ```

4. Edit the config file on */usr/local/etc/sensors-exporter.system* as root (see [configuration section](#configuration))
  ```bash
  sudo nano /usr/local/etc/sensors-exporter.system
  ```

5. Start and enable the service
   ```bash
   sudo systemctl start sensors-exporter.service
   sudo systemctl enable sensors-exporter.servcie
   ```

## Usage
1. Load arduino code source available [here](./sensors.ino) to your arduino

2. Link your arduino to the machine through a usb link

3. Launch a influxdb database  
  this project contains a docker-compose with influxdb, telegraf and grafana image. A dashbaord for Grafana example is available [here](./deployment/grafana-dashboards/dashboard.json).
  ```bash
  make start
  ```

## Configuration
the config can be set in a *yaml* or *json* file or through environnement variables.  
By default, the config path will be *config.yaml* for development environment. You can override it with the flag
"config".

ex:
```sh
./exporter-sensors -config=/my/path.yaml
```

### env
```env
LOCATION= #<{string} ex: "room1" | default: "undefined">

## Arduino
ARDUINO_VERSION= #<{string} ex: "MEGA2650 R3" | default: "undefined">
ARDUINO_MAKER= #<{string} ex: "ELEGOO" | default: "undefined">
ARDUINO_SERIAL_PORT= #<{string} ex: "/dev/ttyACM0 | required>
ARDUINO_BAUD= #<{number} ex: 9600 | default: 9600>

## Influx DB Collector
INFLUXDB_ADDR= #<{string} ex: "http://127.0.0.1:8086" | default:"http://localhost:8086">
INFLUXDB_PRECISION= #<{string} ex: "m" | default: "m">
INFLUXDB_DATABASE= #<{string} ex: "quality" | required>

## MQTT Server
MQTT_ADDR= #<{string} ex: "tcp://mqtt:1883" | default:"tcp://localhost:1883">
MQTT_TOPIC= #<{string} ex: "home" | required>

## HTTP Server
HTTP_HOST= #<{string} ex: "127.0.0.1" | default:"127.0.0.1">
HTTP_PORT= #<{number} ex: 6060 | default:8080>
```

### yaml
```yaml
location: ## <{string} ex: "room1" | default: "undefined">

## Arduino
arduino:
  version: ## <{string} ex: "MEGA2650 R3" | default: "undefined">
  maker: ## <{string} ex: "ELEGOO" | default: "undefined">
  serial_port: ##<{string} ex: "/dev/ttyACM0 | required>
  baud: ## <{number} ex: 9600 | default: 9600>

## MQTT
mqtt:
  addr: ## <{string} ex: "tcp://mqtt:1883" | default: "tcp://localhost:1883">
  topic: ## <{string} ex: "home" | required>

## HTTP Server
http:
  host: ## <{string} ex: "127.O.O.1" | default: "127.0.0.1">
  port: ## <{number} ex: 6060 | default: 8080>

## Influx DB Collector
influx_db:
  addr: ## <{string} ex: "http://127.0.0.1:8086" | default:"http://localhost:8086">
  database: ## <{string} ex: "quality" | required>
  precision: ## <{string} ex: "m" | default: "m">
```

### json
```json
{
   "location": "undefined",
   "arduino": {
      "version": "undefined",
      "maker": "undefined",
      "serial_port": "",
      "baud": 9600
   },
   "influx_db": {
      "addr": "http://localhost:8086",
      "database": "",
      "precision": "m"
   },
   "mqtt": {
      "addr": "tcp://localhost:1883"
   },
   "http": {
      "host": "127.0.0.1",
      "port": 8080
   }
}
```

