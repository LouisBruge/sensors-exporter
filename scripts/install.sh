#! /bin/bash

PROJECT_NAME='sensors-exporter'
BINARY_FILE=$PROJECT_NAME
SYSTEMD_FOLDER='/lib/systemd/system'
BIN_FOLDER='/usr/local/bin'
CONF_FOLDER='/usr/local/etc'

if ! [ $(id -u) = 0 ]
then
  echo "run this script as root user" >&2
  exit 1
fi

if [ ! -e $BINARY_FILE ] 
then
  echo "failed to find $BINARY_FILE file! Build binary before run install script"
  exit 1
fi

if [ -d $BIN_FOLDER ] 
then
  echo "copy binary to $BIN_FOLDER"
  cp -v $BINARY_FILE $BIN_FOLDER
  chmod +x $BIN_FOLDER/$BINARY_FILE
fi

if [ -d $SYSTEMD_FOLDER ]
then
  echo "cop system unit to $SYSTEMD_FOLDER"
  cp -v init/$PROJECT_NAME.service $SYSTEMD_FOLDER
  chmod 644 $SYSTEMD_FOLDER/$PROJECT_NAME.service
fi

if [ -d $CONF_FOLDER ] 
then
  echo "copy config file to $CONF_FOLDER"
  cp -v config/config.example.yaml $CONF_FOLDER/$PROJECT_NAME.yml
fi
