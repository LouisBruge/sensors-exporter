#######################################################################################
# [Config] Image
######################################################################################
image: golang:1.15-alpine

stages:
  - prepare
  - test
  - release

#######################################################################################
# [Config] Cache Policy
#######################################################################################
cache:
  key: $CI_PROJECT_NAME
  policy: pull
  paths:
    - /go/pkg/mod/
    - vendor/
  untracked: false

#######################################################################################
# [Config] Global variables
#######################################################################################
variables:
  PROJECT_NAME: "sensors"
  GO111MODULE: "on"
  CGO_ENABLE: "0"

  GORELEASER_IMAGE: goreleaser/goreleaser:latest
  DOCKER_REGISTRY: $CI_REGISTRY
  DOCKER_USERNAME: $CI_REGISTRY_USER
  DOCKER_PASSWORD: $CI_REGISTRY_PASSWORD

  DS_DEFAULT_ANALYZERS: "gemnasium"

######################################################################################
# [Prepare] Install
######################################################################################
install:
  stage: prepare
  script:
    - go mod vendor
  cache:
    key: $CI_PROJECT_NAME
    policy: pull-push
    paths:
      - /go/pkg/mod/
      - vendor/
    untracked: false
  allow_failure: false
  only:
    refs:
      - web
      - master
      - dev
      - merge_requests
      - tags

####################################################################################
# [Test] Code Quality
####################################################################################
code_quality:
  stage: test
  dependencies:
    - install
  image: golangci/golangci-lint
  script:
    - golangci-lint run --out-format code-climate --issues-exit-code 0 > gl-code-quality-report.json
  artifacts:
    reports:
      codequality: gl-code-quality-report.json
  allow_failure: false
  only:
    refs:
      - web
      - master
      - dev
      - merge_requests
      - tags

##################################################################################
# [Test] Docker Linter
#################################################################################
lint-docker:
  stage: test
  dependencies:
    - install
  image: hadolint/hadolint:latest-debian
  cache: {}
  script:
    - hadolint --ignore DL3018 --ignore DL3025 build/ci/Dockerfile
  allow_failure: false
  only:
    refs:
      - web
      - master
      - dev
      - merge_requests
      - tags

include:
  - template: Dependency-Scanning.gitlab-ci.yml
  - template: Security/License-Scanning.gitlab-ci.yml
  - template: Secret-Detection.gitlab-ci.yml
  - template: SAST.gitlab-ci.yml


##################################################################################
# [Release] Release
##################################################################################
release:
  stage: release
  dependencies:
    - code_quality
    - lint-docker
  image: 
    name: goreleaser/goreleaser:latest
    entrypoint: [""]
  before_script:
    - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  script:
    - /bin/goreleaser release --config=build/ci/.goreleaser.yml
  artifacts:
    paths:
     - dist/
    expire_in: 1 hrs
  allow_failure: false
  only:
    refs:
      - tags
