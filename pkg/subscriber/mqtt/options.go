package mqtt

import (
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
)

// OptionFunc defines option functions for MQTT instances
type OptionFunc func(m *MQTT)

// OptionWithAddress set MQTT endpoint
func OptionWithAddress(a string) OptionFunc {
	return func(m *MQTT) {
		m.mutext.Lock()
		defer m.mutext.Unlock()
		m.address = a
	}
}

// OptionWithTopics appends topics to existing topics
func OptionWithTopics(topics ...string) OptionFunc {
	return func(m *MQTT) {
		m.mutext.Lock()
		defer m.mutext.Unlock()

		m.topics = append(m.topics, topics...)
	}
}

// OptionWithCollector inject a collector
func OptionWithCollector(c collector.Collector) OptionFunc {
	return func(m *MQTT) {
		m.mutext.Lock()
		defer m.mutext.Unlock()

		m.SetCollector(c)
	}
}

// OptionWithLogger to set logger
func OptionWithLogger(log logr.Logger) OptionFunc {
	return func(m *MQTT) {
		m.mutext.Lock()
		defer m.mutext.Unlock()
		m.SetLogger(log)
	}
}
