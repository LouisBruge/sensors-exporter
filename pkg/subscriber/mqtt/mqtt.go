package mqtt

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/models"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
)

var (
	// ErrParsingTopicName is error emit when parsing of topic failed
	ErrParsingTopicName = errors.New("failure on parsing topic name to extract meta data")
)

// MQTT is a concrete implementation of Subscriber that using underlying MQTT protocol
type MQTT struct {
	mutext    sync.Mutex
	address   string
	topics    []string
	collector collector.Collector
	log       logr.Logger
}

// NewMQTT returns a new instance of MQTT Subscriber
func NewMQTT(opts ...OptionFunc) *MQTT {
	m := MQTT{}

	for _, opt := range opts {
		opt(&m)
	}

	return &m
}

// SetLogger set the logger
func (m *MQTT) SetLogger(log logr.Logger) {
	m.log = log.WithName("mqtt")
}

// Logger returns the underlying logger
func (m *MQTT) Logger() logr.Logger {
	return m.log.WithValues("address", m.address)
}

// Connect try to establish a connection with the MQTT Broker
func (m *MQTT) Connect() (mqtt.Client, error) {
	m.Logger().V(1).Info("initialize connection with the Broker")
	opts := mqtt.NewClientOptions().AddBroker(m.address)
	client := mqtt.NewClient(opts)

	if t := client.Connect(); t.Wait() && t.Error() != nil {
		m.Logger().Error(t.Error(), "failed to establish connection with the Broker")
		return nil, t.Error()
	}

	m.Logger().V(1).Info("connection establish with the Broker")
	return client, nil
}

// SetCollector inject a collector inside the subscriber
func (m *MQTT) SetCollector(c collector.Collector) {
	m.mutext.Lock()
	defer m.mutext.Unlock()
	m.collector = c
}

// Subscribe open a connection with the MQTT Broker and subscriber to each declare topics
func (m *MQTT) Subscribe() error {
	client, err := m.Connect()
	if err != nil {
		return err
	}

	for _, topic := range m.topics {
		if t := client.Subscribe(m.formatTopic(topic), 0, m.onMessage); t.Wait() && t.Error() != nil {
			m.Logger().WithValues("topic", m.formatTopic(topic)).Error(t.Error(), "failed to subscribe on topic")
			return t.Error()
		}
		m.Logger().V(1).WithValues("topic", m.formatTopic(topic)).Info("new subscription on topic")
	}

	return nil
}

func (m *MQTT) onMessage(client mqtt.Client, msg mqtt.Message) {
	defer msg.Ack()

	log := m.Logger().WithValues("topic", msg.Topic(), "payload", string(msg.Payload()))

	value, err := strconv.ParseFloat(string(msg.Payload()), 32)
	if err != nil {
		log.Error(err, "failed to parser payload value")
		return
	}

	metaData, err := m.parseTopic(msg.Topic())
	if err != nil {
		log.Error(err, "failed to extract meta data to the topic")
		return
	}

	metric := models.Metric{
		Value:    float32(value),
		Location: metaData[0],
		Sensor:   metaData[1],
		Kind:     metaData[2],
	}

	if e := m.collector.Collect(metric); e != nil {
		return
	}
}

func (m *MQTT) formatTopic(t string) string {
	return fmt.Sprintf("%s/#", t)
}

func (m *MQTT) parseTopic(t string) ([]string, error) {
	for _, topic := range m.topics {
		if strings.HasPrefix(t, topic) {
			str := strings.Replace(t, fmt.Sprintf("%s/", topic), "", 1)
			metaData := strings.Split(str, "/")
			if len(metaData) < 3 {
				return nil, ErrParsingTopicName
			}
			return metaData, nil
		}
	}
	return nil, ErrParsingTopicName
}
