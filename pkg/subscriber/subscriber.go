package subscriber

import (
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
)

// Subscriber defines methods exported by a subscriber
type Subscriber interface {
	Subscribe() error
	SetCollector(collector.Collector)
	SetLogger(logr.Logger)
}
