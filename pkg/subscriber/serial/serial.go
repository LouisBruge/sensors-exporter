package serial

import (
	"bufio"
	"encoding/json"

	"github.com/go-logr/logr"
	"github.com/tarm/serial"
	"gitlab.com/LouisBruge/sensors-exporter/models"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
)

// Serial is a concrete implementation of the Subcriber interface
// that using underlying Tarm Serial reader
type Serial struct {
	port      string
	baud      int
	collector collector.Collector
	log       logr.Logger
}

// NewSerial returns a new Serial instance
func NewSerial(opts ...OptionFunc) *Serial {
	s := Serial{}
	for _, opt := range opts {
		opt(&s)
	}

	return &s
}

// SetLogger set the logger
func (s *Serial) SetLogger(l logr.Logger) {
	s.log = l.WithName("serial")
}

// SetCollector set the collector
func (s *Serial) SetCollector(c collector.Collector) {
	s.collector = c
}

// Logger returns the underlying logger
func (s *Serial) Logger() logr.Logger {
	return s.log.WithValues("port", s.port, "baud", s.baud)
}

// Subscribe initialize the connection on the serial port and handle every incoming message
// from the serial port
func (s *Serial) Subscribe() error {
	s.Logger().V(1).Info("initialize connection")

	serialPort, err := serial.OpenPort(&serial.Config{
		Name: s.port,
		Baud: s.baud,
	})
	if err != nil {
		return err
	}
	s.Logger().V(1).Info("connection establish")

	if e := serialPort.Flush(); e != nil {
		return err
	}
	s.Logger().V(2).Info("flush serial port")

	s.Logger().V(1).Info("start scanning")
	scanner := bufio.NewScanner(serialPort)
	for scanner.Scan() {
		if e := s.Scan(scanner.Text()); e != nil {
			s.Logger().Error(e, "failed to scan")
		}
	}

	return nil
}

// Scan incoming message
func (s *Serial) Scan(payload string) error {
	s.Logger().V(2).Info("incoming message...", "payload", payload)
	metrics, err := s.parseToJSON(payload)
	if err != nil {
		return err
	}

	if e := s.collector.Collect(metrics...); e != nil {
		return e
	}
	return nil
}

// parserToJSON parses the payload into a slice of metric
func (s *Serial) parseToJSON(payload string) ([]models.Metric, error) {
	var buf models.Measure
	if e := json.Unmarshal([]byte(payload), &buf); e != nil {
		return nil, e
	}
	return buf.Metrics, nil
}
