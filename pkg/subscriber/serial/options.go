package serial

import (
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
)

type OptionFunc func(*Serial)

func OptionWithBaud(baud int) OptionFunc {
	return func(s *Serial) {
		s.baud = baud
	}
}

func OptionWithPort(port string) OptionFunc {
	return func(s *Serial) {
		s.port = port
	}
}

func OptionWithCollector(c collector.Collector) OptionFunc {
	return func(s *Serial) {
		s.collector = c
	}
}

// OptionWithLogger to set logger
func OptionWithLogger(log logr.Logger) OptionFunc {
	return func(s *Serial) {
		s.SetLogger(log)
	}
}
