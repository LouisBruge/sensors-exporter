package influx

import (
	"time"

	"github.com/go-logr/logr"
	influx "github.com/influxdata/influxdb1-client/v2"
	"gitlab.com/LouisBruge/sensors-exporter/models"
)

// OptionFunc type functionnal parameters for InfluxCollector
type OptionFunc func(c *Influx)

// OptionWithAddress option to set influx collector endpoint
func OptionWithAddress(addr string) OptionFunc {
	return func(c *Influx) {
		c.Address = addr
	}
}

// OptionWithDatabase option to set influx database
func OptionWithDatabase(db string) OptionFunc {
	return func(c *Influx) {
		c.Database = db
	}
}

// OptionWithPrecision option to set influx database precision
func OptionWithPrecision(precision string) OptionFunc {
	return func(c *Influx) {
		c.Precision = precision
	}
}

// OptionWithTimeout option to set influx client timeout
func OptionWithTimeout(t time.Duration) OptionFunc {
	return func(c *Influx) {
		c.Timeout = t
	}
}

// OptionWithLogger to set logger
func OptionWithLogger(log logr.Logger) OptionFunc {
	return func(c *Influx) {
		c.SetLogger(log)
	}
}

// Influx is a concrete implementation of Collector interface
// with underlying influx server
type Influx struct {
	Address   string
	Database  string
	Precision string
	Timeout   time.Duration
	logger    logr.Logger
}

// NewInfluxCollector initialize a new instance of influx collector
// that push metrics through http requests
func NewInfluxCollector(opts ...OptionFunc) *Influx {
	collector := &Influx{}

	for _, opt := range opts {
		opt(collector)
	}

	return collector
}

// SetLogger set logger
func (c *Influx) SetLogger(log logr.Logger) {
	c.logger = log.WithName("influx")
}

// Logger returns the underlying logger
func (c *Influx) Logger() logr.Logger {
	return c.logger.WithValues("address", c.Address, "database", c.Database, "precision", c.Precision)
}

// Collect aggreage metrics before send request to influx database
func (c *Influx) Collect(metrics ...models.Metric) error {
	bp, err := influx.NewBatchPoints(influx.BatchPointsConfig{
		Database:  c.Database,
		Precision: c.Precision,
	})
	if err != nil {
		return err
	}

	for _, metric := range metrics {
		tags := map[string]string{
			"sensor":   metric.Sensor,
			"location": metric.Location,
		}

		pt, err := influx.NewPoint(
			metric.Kind,
			tags, map[string]interface{}{"value": metric.Value},
			time.Now(),
		)

		if err != nil {
			return err
		}

		c.Logger().V(2).Info("append a new point", "point", pt)
		bp.AddPoint(pt)
	}
	return c.pushToServer(bp)
}

// pushToServer open a new http request in order to push a batch of point to the server
func (c *Influx) pushToServer(bp influx.BatchPoints) error {
	client, err := influx.NewHTTPClient(influx.HTTPConfig{
		Addr:    c.Address,
		Timeout: c.Timeout,
	})
	if err != nil {
		return err
	}

	defer client.Close()

	c.Logger().V(1).Info("push new batch of point to the back-end", "points", bp)
	if e := client.Write(bp); e != nil {
		return e
	}
	return nil
}
