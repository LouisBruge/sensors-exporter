package collector

import (
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/models"
)

// Collector defines methods exposed by a Collector
type Collector interface {
	Collect(metrics ...models.Metric) error
	SetLogger(logr.Logger)
}
