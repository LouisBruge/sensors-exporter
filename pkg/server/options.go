package server

// OptionFunc option functions
type OptionFunc func(*HTTP)

// OptionWithPort option to set port for web server
func OptionWithPort(p int) OptionFunc {
	return func(s *HTTP) {
		s.port = p
	}
}

// OptionWithHost option to set host for web server
func OptionWithHost(h string) OptionFunc {
	return func(s *HTTP) {
		s.host = h
	}
}
