package server

import (
	"net/http"
	"strconv"
	"strings"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

// HTTP implement a http server
type HTTP struct {
	host string
	port int
}

// NewHTTPServer return a new instance of HTTPServer
func NewHTTPServer(opts ...OptionFunc) *HTTP {
	s := &HTTP{}

	for _, opt := range opts {
		opt(s)
	}

	return s
}

// Listen listens incoming http requests
func (s *HTTP) Listen() error {
	http.Handle("/metrics", promhttp.Handler())
	return http.ListenAndServe(s.addr(), nil)
}

// addr returns http server address
func (s HTTP) addr() string {
	str := strings.Builder{}
	str.WriteString(s.host)
	str.WriteRune(':')
	str.WriteString(strconv.Itoa(s.port))

	return str.String()
}
