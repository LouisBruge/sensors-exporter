package exporter

import (
	"log"
	"sync"

	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/models"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/subscriber"
)

// Exporter defines methods exported by a exporter
type Exporter interface {
	Listen()
}

// exporter is a concrete implementation of Exporter interface
// using underlying serial port and influx db
type exporter struct {
	mutex       sync.Mutex
	config      models.Config
	collector   collector.Collector
	subscribers []subscriber.Subscriber
	logger      logr.Logger
}

// NewExporter returns a new exporter from the provided config
func NewExporter(cfg models.Config, opts ...OptionFunc) (Exporter, error) {
	ex := exporter{
		config: cfg,
	}

	for _, opt := range opts {
		opt(&ex)
	}

	return &ex, nil
}

// Listen await for messages sended through the serial port
func (e *exporter) Listen() {
	wg := sync.WaitGroup{}
	for _, s := range e.subscribers {
		wg.Add(1)
		s.SetCollector(e.collector)
		go func(sub subscriber.Subscriber, w *sync.WaitGroup) {
			defer w.Done()
			if e := sub.Subscribe(); e != nil {
				log.Print(e)
			}
		}(s, &wg)
	}
	wg.Wait()
}
