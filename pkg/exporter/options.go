package exporter

import (
	"github.com/go-logr/logr"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/collector"
	"gitlab.com/LouisBruge/sensors-exporter/pkg/subscriber"
)

// OptionFunc defines option functions for exporter
type OptionFunc func(*exporter)

// OptionWithCollector assign a collector to the exporter
func OptionWithCollector(c collector.Collector) OptionFunc {
	return func(e *exporter) {
		e.mutex.Lock()
		defer e.mutex.Unlock()
		e.collector = c
	}
}

// OptionWithSubscribers assign severial subscribers to the exporter
func OptionWithSubscribers(subscribers ...subscriber.Subscriber) OptionFunc {
	return func(e *exporter) {
		e.mutex.Lock()
		defer e.mutex.Unlock()
		e.subscribers = append(e.subscribers, subscribers...)
	}
}

// OptionWithLogger enable to inject a logger
func OptionWithLogger(log logr.Logger) OptionFunc {
	return func(e *exporter) {
		e.mutex.Lock()
		defer e.mutex.Unlock()
		e.logger = log
	}
}
